let defaultState = {
    response: '',
    q: '',
    type: 'track'

}

const mainReduer = (state = defaultState, action)=>{
    if(action.type==='FILTER'){
        return {
            ...state,
            response: action.response,
            search: action.state
        }
    }else{
        return {
            ...state
        }
    }
}

export default mainReduer;