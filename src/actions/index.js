import axios from "axios";

const token = 'BQCGWK3QYerwQBiYI77CWoVCMViUKNqxSbLgsM47ZJw1eov6FGoPD8gz_vTNyDLCUavxvSh6NmakDLertobyQOHCOJRfG_qg6vfWZJUhu4avf_rBreOjab2nN2u3UT9NCiBS5ykwvd4D5jlorcntMByyJ6KRPCkht6xFJQ'

export function filter(state){
    return(dispatch)=>{
        return axios.get("https://api.spotify.com/v1/search?q="+state.q+"&type="+state.type, {headers: {"Authorization":`Bearer ${token}`}}).then((response)=>{
            let res;

            switch(state.type) {
                case 'artist':
                    res = response.data.artists;
                    break;
                case 'album':
                    res = response.data.albums
                    break;
                default:
                    res = response.data.tracks
              }

            dispatch(dispatchFilter(res, state));
        })
    }
}

export function dispatchFilter(response, state){
    return {
        type: 'FILTER',
        state: state,
        response: response
    }
}


export function getTracks(id){
    return (dispatch)=>{
        return axios.get("https://api.spotify.com/v1/albums/"+id+"/tracks", {headers: {"Authorization":`Bearer ${token}`}}).then((response)=>{
            return {
                type: 'GET_TRACKS',
                tracks: response
            }
        })
    }
}

export function dispatchTracks(response){
    return {
        type: 'GET_TRACKS',
        tracks: response
    }
}

// export function getAlbums(id){
//     return (dispatch)=>{
//         return axios.get("https://api.spotify.com/v1/albums/"+id+"/tracks", {headers: {"Authorization":`Bearer ${token}`}}).then((response)=>{
//             dispatch(dispatchAlbums(response));
//         })
//     }
// }

// export function dispatchAlbums(response){
//     return {
//         type: 'GET_ALBUMS',
//         tracks: response
//     }
// }