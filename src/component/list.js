import React from 'react';
import {connect} from 'react-redux';
import * as actions from '../actions/index';

const img = require('../img/nao-disponivel.jpg');

class List extends React.Component{
    constructor(props) {
        super(props);
        
        this.state = {
            q: '',
            type: 'track'
        };

        // this.handleChange = this.handleChange.bind(this);
        // this.handleSubmit = this.handleSubmit.bind(this);
        // this.renderPopularity = this.renderPopularity.bind(this);
      }   

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };
    
    handleSubmit = e => {
        e.preventDefault();
        this.props.filter(this.state);
        // this.handleReset();
    };

    handleReset = () => {
        this.setState({
            q: '',
            type: 'track'
        });
    };

    render(){
        const response = this.props.response
        const list = this.props.response.items
        const search = this.props.search

        const millisToMinutesAndSeconds = (ms) => {
            var minutes = Math.floor(ms / 60000);
            var seconds = ((ms % 60000) / 1000).toFixed(0);
            return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
        }

        const renderPopularity = (item) =>{
            if(item >= 80)
                return ' HOT'
            else if(item >= 60 && item < 80)
                return ' COOL'
            else if(item >= 30 && item < 60)
                return ' REGULAR'
            else
                return ' UNDERGROUND'
        }

        const renderList = (list, type) => {
            switch(type) {
                case 'artist':
                    return list.map(function(item, index){
                        return(
                            <li className="media mb-3" key={index}>
                                { item.images.length > 0 ?
                                    <img src={item.images[0].url} height="150" width="150" className="mr-3" alt={item.name} /> :
                                    <img src={img} height="150" width="150" className="mr-3" alt="Imagem não disponível" />
                                }
                                <div className="media-body">
                                    <h4 className="mt-0 mb-1">Artista: {item.name}</h4>
                                    <h5>Gêneros: {item.genres.join(", ")}</h5>
                                    <p>Popularidade:{renderPopularity(item.popularity)}
                                    
                                    </p>
                                </div>
                            </li>
                        )
                    })
                case 'album':
                    return list.map(function(item, index){
                        return(
                            <li className="media mb-3" key={index}>
                                <img src={item.images[0].url} height="150" width="150" className="mr-3" alt={item.name} />
                                <div className="media-body">
                                    <h4 className="mt-0 mb-1">Álbum: {item.name}</h4>
                                    <h5>Artista: {item.artists[0].name === 'Vários intérpretes' ? 'Vários artistas' : item.artists[0].name}</h5>
                                    <span>Disponibilidade: {item.available_markets.join(", ")}</span>
                                </div>
                                
                            </li>
                        )
                    })
                default:
                    return list.map(function(item, index){
                        return (
                            <li className="media mb-3" key={index}>
                                <img src={item.album.images[0].url} height="150" width="150" className="mr-3" alt={item.name} />
                                <div className="media-body">
                                    <h4 className="mt-0 mb-1">Música: {item.name}</h4>
                                    <h5>Artista:&nbsp;
                                        {item.artists.length > 1 ? 
                                            item.artists.map(function(artist){
                                                return artist.name;
                                            }).join(", ") :
                                            item.artists[0].name
                                        }
                                    </h5>
                                    <h5>Álbum: {item.album.name}</h5>
                                    <p>Duração: {millisToMinutesAndSeconds(item.duration_ms)}</p>
                                </div>
                            </li>
                        )   
                    })
            }
        }

        return(
            <div className="row">
                <div className="col-sm-12">
                    <h1>Busca Spotify</h1>

                    <hr />

                    <form onSubmit={this.handleSubmit}>
                        <div className="input-group mb-3">
                            <input type="text" className="form-control" aria-label="Text input with dropdown button" placeholder='Digite o termo que deseja procurar...' name="q" onChange={this.handleChange} value={this.state.q} required />
                            
                            <div className="input-group-append">
                                <select className="custom-select rounded-0" value={this.state.type} onChange={this.handleChange} name="type">
                                    <option value="track">Música</option>
                                    <option value="artist">Artista</option>
                                    <option value="album">Álbum</option>
                                </select>
                            
                                <button type="submit" className="btn btn-success" id="button-addon2">Buscar</button>
                            </div>
                        </div>
                    </form>      
    
                    <ul className="list-group-flush w-100">
                        { response === ''  || response === undefined ?
                            <li className="list-group-item">Nenhum registro encontrado.</li>
                        : 
                            renderList(list, search.type)
                        }
                    </ul>
                </div>
            </div>
        )
    }
}

const mapStateToProps=(state)=>{
    return state
}

export default connect(mapStateToProps, actions)(List);