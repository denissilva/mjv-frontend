import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware} from "redux";
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import reducers from './reducers/index';

import Container from './container/container';

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

let store = createStore(reducers, applyMiddleware(thunk));

class App extends React.Component{
    render(){
        return(
            <div className="container">
                <Container />
            </div>
        )
    }
}

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
