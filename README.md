MJV Frontend

Tarefas executadas:

    - Criação app API Spotify;
    - Configuração e geração token de acesso (https://github.com/spotify/web-api-auth-examples);
    - Criação do projeto React (create-react-app);
    - Linkando com git (bitbucket);
    - Configuração de deploy automático com heroku (bitbucket pipelines)
    - Adicionando redux react-redux e axios;
    - Adicionando bootstrap;
    - Adicionando react-thunk;
    - Criação de reducers e actions;
    - Consumindo API;
    - Passando parâmetros para pesquisa;
    - Renderizando lista;
    - Detalhes de artistas, álbum e faixas;
    - Detalhes artistas popularidade;
    - Convertendo ms para minutos e segundos;

Obrigatórios:
    - Totalmente responsivo;
    - Projeto em CVS (bitbucket);
    - Arquivo Readme;
    - Detalhamento das tarefas;
    - Detalhamento de tarefas executadas;
    - Último commit até às 23h de 27/02/2019

Desejáveis:
    - ES6+;
    - React + Redux;
    - Hospedagem Live Dome (heroku);
    - Bootstrap;
    - Suporte Chrome 72+;

Obs: infelizmente não consegui implementar a autenticação da api, sendo assim, caso seja necessário atualizar o token, por favor me avisem.